﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;　　//System.Netの参照設定が必要です

namespace Translater_with_Android
{
    public partial class MainForm : Form
    {
        private Command command = new Command();

        public MainForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (serialPortsControl1.exSerialPort1.IsOpen)
            {
                ///TODO:Startボタン入力時
                serialPortsControl1.ExWrite(command.MakeStart());
                Invoke((Action)delegate()
                {
                    listBox1.Items.Clear();
                    listBox1.Items.Add("[" + DateTime.Now.ToString("HH:mm:ss") + "] Start");
                });
            }
        }

        private void serialPortsControl1_DataReceived(object sender, EventArgs e)
        {
            byte[] buf = serialPortsControl1.ExRead();

            System.Threading.Thread.Sleep(100);

            try
            {
                DATA data = new DATA(buf);
                switch (data.Type)
                {
                    case Type.START_RES:

                        break;
                    case Type.STOP_RES:
                        break;
                    case Type.MSG_SUCSESS:
                        string[] msg = data.Message.Split(',');
                        Invoke((Action)delegate()
                        {
                            for (int Loop = 0; Loop < 5;Loop ++)
                            {
                                listBox1.Items.Add("[" + DateTime.Now.ToString("HH:mm:ss") + "] " + msg[Loop]);
                            }
                        });
                        break;
                    case Type.MSG_ERROR:
                        Invoke((Action)delegate()
                        {
                            listBox1.Items.Add("[" + DateTime.Now.ToString("HH:mm:ss") + "] Error");
                        });
                        break;
                    case Type.MOTOR_AHEAD:
                        
                        break;
                    case Type.MOTOR_STOP:

                        break;
                    case Type.MOTOR_LEFT:

                        break;
                    case Type.MOTOR_RIGHT:

                        break;
                    case Type.MOTOR_BACK:

                        break;
                    case Type.MOTOR_DEMO:

                        break;
                    default:
                        break;
                }
            }
            catch (InvalidCastException)
            {

            }
        }

        private void serialPortsControl1_SerialDisConnect(object sender, EventArgs e)
        {
            MessageBox.Show("COMポートがクローズしました。");
        }
    }
}
