﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Translater_with_Android
{
    public class Command
    {
        private const byte ST = 0x11;

        /// <summary>
        /// スタートコマンドを生成する
        /// </summary>
        public byte[] MakeStart()
        {
            return Make(Type.START_REQ);

        }

        /// <summary>
        /// ストップコマンドを生成する
        /// </summary>
        /// <returns></returns>
        public byte[] MakeStop()
        {
            return Make(Type.STOP_REQ);
        }

        /// <summary>
        /// コマンドを生成する
        /// </summary>
        /// <param name="type"></param>
        private byte[] Make(Type type)
        {
            byte[] data = new byte[4];
            data[0] = ST; // スタートデリミタ
            data[1] = (byte)type; // コマンドタイプ
            data[2] = 0; // データ長
            data[3] = 0; // データ長
            return data;
        }

        /// <summary>
        /// コマンドを確認を行う
        /// </summary>
        public DATA Check(byte[] data)
        {
            return new DATA(data);
        }
    }

    /// <summary>
    /// 応答データ
    /// </summary>
    public class DATA
    {
        private const byte ST = 0x11;

        public readonly Type Type;
        public readonly string Message;

        public DATA(byte[] data)
        {
            if (data[0] == ST && data.Length == (data[2] * 256 + data[3]) + 4)
            {
                this.Type = (Type)data[1];
                
                if(Type == Type.MSG_SUCSESS){
                    byte[] buf = new byte[data[2] * 256 + data[3]];

                    for(int Loop = 0;Loop < buf.Length;Loop ++){
                        buf[Loop] = data[Loop + 4];
                    }

                   Message = Encoding.GetEncoding("Shift_JIS").GetString(buf);
                }
            }
            else
            {
                throw new InvalidCastException("解釈できないデータです");
            }
        }
    }

    public enum Type : byte
    {
        NONE = 0x00,
        START_REQ = 0x40, // 開始
        START_RES = 0x41, // 開始
        STOP_REQ = 0x50, //終了
        STOP_RES = 0x51, //終了
        MSG_SUCSESS = 0x60, //メッセージ
        MSG_ERROR = 0x61, //メッセージ
        MOTOR_AHEAD = 0x70, // 前
	    MOTOR_RIGHT = 0x71, // 右
	    MOTOR_LEFT = 0x72, // 左
	    MOTOR_STOP = 0x73, // 停止
	    MOTOR_BACK = 0x74, // 後退
	    MOTOR_DEMO = 0x75, // デモ
    }

}
